﻿define([
        "durandal/app",
        "appSettings"
    ],
    function(app, appSettings) {
        {
            app.urlBase = appSettings.urlBase + "api/";

            console.log(app.urlBase);

            app.api = function (settings) {
                settings.url = app.url('/api' + settings.url);

                var ajaxOptions = $.extend(settings, {
                    headers: {
                        Accept: "application/json",
                    },
                    dataType: "json",
                });

                return $.ajax(ajaxOptions);
            };

        }
    });