﻿define(['jquery', 'knockout', 'lodash', 'appSettings'], function ($, ko, _, appSettings) {

    var vm = function() {
        
        var self = this;

        self.credits = ko.observable();

        var countries = ko.observableArray([]);



        self.displayCountries = ko.computed(
            function () {
                var filtered = countries(); 
                return filtered;
            }
        );


    
        var loadCountries = function () {
            var deferred = $.Deferred();
            $.ajax({
                url: appSettings.apiUrlBase + "country"
            }).done(function(result) {
                countries(result);

                deferred.resolve(result);
           
            });

            return deferred.promise();

        };

        self.players = ko.observableArray([]);
        self.isPlayerFull = ko.computed(function () {

            console.log(self.players().length);
            if (self.players().length == 1) {
                return true;
            } else {
                return false;
            }
        });

        self.chosenPlayerToWin = ko.observable();
        self.chosen = ko.computed(function () {
            
            if (self.chosenPlayerToWin()) {
               
                return self.chosenPlayerToWin().Name;
            }
            return "";
        });

        self.bet = ko.observable(0);

        self.winner = ko.observable();

        self.getWinnerName = ko.observable("");

        self.selectPlayer = function (data, event) {
            
            var players = self.players();
            console.log("You selected " + data.Name);
            

            if (players.length > 1) {
                alert('Only two countries can be played at time');
                return;
            } else {
                _.assign(data, {
                    oddsDisplay: ko.observable(""),
                    odds: 0,
            });

                
                players.push(data);
                
            }

            console.log("Players " + self.players().length);
            
            self.players(players);
        };

        var generateOdds = ko.computed(function() {

            if (self.players().length === 2) {
                var players = self.players();
                console.debug(players);
                var top = 100, bottom = 1;
                var odds = Math.floor(Math.random() * (1 + top - bottom)) + bottom;
                players[1].oddsDisplay(odds + "% chance to win.");
                players[1].odds = odds;

                players[0].oddsDisplay((100 - odds) + "% chance to win.");
                players[0].odds = (100 - odds);
                

                self.players(players);
            }
        });

        self.play = function () {
            if (self.bet() > self.credits()) {
                alert("invalid amount");
                return;
            }
            var top = 1, bottom = 0;

            // should get the 
            var w = Math.floor(Math.random() * (1 + top - bottom)) + bottom;
            

            console.log("random " + w);
            
            var winner = self.players()[w];
           
            console.log("Winner is " + winner.Name);
            self.getWinnerName("Winner is " + winner.Name);
            self.winner(winner);

            
        };

        self.allIn = function () {

            var credits = self.credits();
            self.bet(credits);
        };

        var computeWinnings = function (bet, odds) {

            console.log("COMPUTE " + bet + "/" + odds);
            return Math.floor(bet * ((100-odds) / 100));
        };

        self.winningsDisplay = ko.computed(function() {
            if (self.winner()) {
                if (self.winner().Id == self.chosenPlayerToWin().Id) {
                    var winnings = computeWinnings(self.bet(), self.chosenPlayerToWin().odds);
                    self.credits(self.credits() + winnings);
                    return "You won $" + winnings ;
                } else {
                    var loss = computeWinnings(self.bet(), self.chosenPlayerToWin().odds);
                    self.credits(self.credits() - loss);

                    var credits = self.credits();
                    if (credits < 0) self.credits(0);


                    return "You lost $" + loss;
                }
                self.bet(0);
              
            } else {
                return "";
            }
        });

        

        self.hasChosen = ko.computed(function() {
            if (self.players().length > 0) {
                return true;
            }
            return false;
        });
        self.hasPicked = ko.computed(function () {
            if (self.chosenPlayerToWin()) {
                return true;
            }
            return false;
        });
        self.hasWinner = ko.computed(function () {
            if (self.winner()) {
                return true;
            }
            return false;
        });
        self.reset = function() {

            self.players([]);
            self.chosenPlayerToWin(null);
            self.winner(null);

            self.bet(0);
            self.getWinnerName("");
            
        };
        self.activate = function () {
            self.showSplash(true);
            window.vm = self; // for testing
            self.credits(100);
            self.bet(20);
            $.when(loadCountries()).done(function() {
                self.showSplash(false);
            });

        };

        

    };


    $.extend(vm.prototype, {
            showSplash: ko.observable(false),
            compositionComplete: function () {

                var $scrollingDiv = $("#scrollingDiv");
                
                
                $(window).scroll(function () {
                        $scrollingDiv
                            .stop()
                            .animate({ "marginTop": ($(window).scrollTop() + 30) + "px" }, "slow");
                    });
                
            },
            
        });



    return vm;

});