﻿define(function() {
    var ctor = function () {
        this.displayName = 'Single Page App';
        this.description = 'This website is a single page app made using ASP.NET Web API and Durandal Framework';
        this.features = [
            'Use Entity Framework Code-First approach',
            'Use SQL Server CE / LocalDb database',
            'Use Unit of Work (UoW) and Repository Pattern in the Data Layer',
            'Use ASP.NET Web API to expose data to the Client',
            'Use Microsofts UNITY for dependency injection/ IOC',
            'Use Durandal for the UI Framework',
            'Use KnockoutJS for UI databinding',
            'Use LodashJs for data manipulation in the UI',
        ];
    };

    //Note: This module exports a function. That means that you, the developer, can create multiple instances.
    //This pattern is also recognized by Durandal so that it can create instances on demand.
    //If you wish to create a singleton, you should export an object instead of a function.
    //See the "flickr" module for an example of object export.

    return ctor;
});
