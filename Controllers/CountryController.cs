﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.UI;
using KSabay_Core;

using KSabay_Entities;

namespace KSabay_SPA.Controllers
{
    
    public class CountryController : ApiController
    {
        private readonly IUnitOfWork _unitOfWork;

        public CountryController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public Country[] GetCountries()
        {
            return _unitOfWork.CountryRepository.GetCountries().ToArray();
        }
    }
}
