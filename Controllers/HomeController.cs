﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using KSabay_Core;

namespace KSabay_SPA.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;


        public HomeController()
            : this(new UnitOfWork())
        {

        }

        public HomeController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        //public ActionResult Index()
        //{
        //    var student = _unitOfWork.StudentRepository.GetStudentByID(1);
            
        //    var title = "Home Page";
        //    if (student != null)
        //    {
        //        title = student.Name;
        //    }

        //    ViewBag.Title = title;

            
        //    return View();
        //}
    }
}
