using KSabay_Core;
using KSabay_SPA.Controllers;
using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;

namespace KSabay_SPA
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterInstance<HttpConfiguration>(GlobalConfiguration.Configuration);

            container.RegisterType<ValuesController>();
            container.RegisterType<HomeController>();

            container.RegisterType<IUnitOfWork, UnitOfWork>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}